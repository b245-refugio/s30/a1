// 1 count total number of fruits on sale.
db.fruits.aggregate([
{$match: {onSale:true}},
{$count:"fruitsOnSale"}
]);

// 2 count total number of fruits more than or equal to 20

db.fruits.aggregate([
{$match: {stock: {$gte:20}}},
{$count:"enoughStock"}
]);

// 3 average price of fruits onSale per supplier

db.fruits.aggregate([
{$match: {onSale:true}},
{$group:{_id: "$supplier_id", avg_price:{$avg:"$price"}}}
]);

// 4 highest price of a fruit per supplier

db.fruits.aggregate([
{$match: {onSale:true}},
{$group:{_id: "$supplier_id", max_price:{$max:"$price"}}}
]);

// 5 lowest price of a fruit per supplier

db.fruits.aggregate([
{$match: {onSale:true}},
{$group:{_id: "$supplier_id", min_price:{$min:"$price"}}}
]);

